package Sheridan;

public class prime {




	
	
	public static boolean isPrime( int number ) {
		for ( int i = 2 ; i <= number /2 ; i ++) {
			if ( number % i == 0  ) {
				return false;
			}
		}
		return true;
 	}

	public static void main(String[] args) {
		

		System.out.println( "Number 29 is prime? " + prime.isPrime( 29 ) );
		System.out.println( "Number 29 is prime? " + prime.isPrime( 30 ) );		
		System.out.println( "Number 33 is prime? " + prime.isPrime( 33 ) );
		System.out.println( "Number 34 is prime? " + prime.isPrime( 34 ) );	
		System.out.println( "Number 35 is prime? " + prime.isPrime( 35 ) );		
		System.out.println( "Number 36 is prime? " + prime.isPrime( 36 ) );				
		System.out.println( "Number 50 is prime? " + prime.isPrime( 50 ) );	// change 1			
		System.out.println( "Number 51 is prime? " + prime.isPrime( 51 ) );	// change 2					
		System.out.println( "Number 54 is prime? " + prime.isPrime( 54 ) );
	}

}
